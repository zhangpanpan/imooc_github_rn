import React from 'react';
import PopularPage from "../pages/PopularPage";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import TrendingPage from "../pages/TrendingPage";
import Ionicons from "react-native-vector-icons/Ionicons";
import FavoritePage from "../pages/FavoritePage";
import MyPage from "../pages/MyPage";
import Entypo from "react-native-vector-icons/Entypo";
import {createAppContainer} from 'react-navigation';
import {createBottomTabNavigator, BottomTabBar} from 'react-navigation-tabs';

const TABS ={ // 配置导航路由
    PopularPage: {
        screen: PopularPage,
        navigationOptions: {
            tabBarLabel: '最热',
            tabBarIcon: (tintColor, focused) => (
                <MaterialIcons
                    name={'whatshot'}
                    size={26}
                    style={{color: tintColor}}
                />
            ),
        }
    },
    TrendingPage: {
        screen: TrendingPage,
        navigationOptions: {
            tabBarLabel: '趋势',
            tabBarIcon: (tintColor, focused) => (
                <Ionicons
                    name={'md-trending-up'}
                    size={26}
                    style={{color: tintColor}}
                />
            ),
        }
    },
    FavoritePage: {
        screen: FavoritePage,
        navigationOptions: {
            tabBarLabel: '收藏',
            tabBarIcon: (tintColor, focused) => (
                <MaterialIcons
                    name={'favorite'}
                    size={26}
                    style={{color: tintColor}}
                />
            ),
        }
    },
    MyPage: {
        screen: MyPage,
        navigationOptions: {
            tabBarLabel: '我的',
            tabBarIcon: (tintColor, focused) => (
                <Entypo
                    name={'user'}
                    size={26}
                    style={{color: tintColor}}
                />
            ),
        }
    },
};

export default class DynamicTabNavigator extends React.Component {
    constructor(props) {
        super(props);
        console.disableYellowBox = true; // 关闭黄色警告弹窗
    }

    _tabNavigator() {
        const {FavoritePage, MyPage, PopularPage, TrendingPage} = TABS;
        /**
         * 模拟服务端动态下发tab
         * */
        const tabs = {PopularPage, TrendingPage, FavoritePage, MyPage};
        // PopularPage.navigationOptions.tabBarLabel = "最热1"; // 根据服务端动态配置tab属性
        return createAppContainer(createBottomTabNavigator(tabs, {
            tabBarComponent: TabBarComponent
        }));
    }

    render() {
        const Tabs = this._tabNavigator();
        return <Tabs />;
    };
}

class TabBarComponent extends React.Component {
    constructor(props) {
        super(props);
        this.theme = {
            tintColor: props.activeTintColor, // tab颜色
            updateTime: new Date().getTime(),
        };
    }

    render() {
        const {routes, index} = this.props.navigation.state;
        if(routes[index] && routes[index].params) {
            const {theme} = routes[index].params;
            // 以最新的更新时间为准
            if(theme && theme.updateTime > this.theme.updateTime) {
                this.theme = theme;
            }
        }
        return <BottomTabBar
            {...this.props}
            activeTintColor={this.theme.tintColor || this.props.activeTintColor}

        />;
    }
}
