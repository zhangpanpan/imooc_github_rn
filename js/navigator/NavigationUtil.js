export default class NavigationUtil {
  // 重置路由跳转到首页
  static resetToHomePage(params) {
    const {navigation} = params;
    navigation.navigate('Main');
  }

  /**
   * 页面跳转方法
   * */
  static goPage(params, page) {
    const navigation = NavigationUtil.navigation;
    if(!navigation || !navigation.navigate) {
      console.log('NavigationUtil.navigation can not be null!');
      return ;
    }
    navigation.navigate(page, { ...params });
  }
}
