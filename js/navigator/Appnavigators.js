import {createAppContainer, createSwitchNavigator} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import FavoritePage from "../pages/FavoritePage";
import HomePage from "../pages/HomePage";
import MyPage from "../pages/MyPage";
import PopularPage from "../pages/PopularPage";
import TrendingPage from "../pages/TrendingPage";
import WelcomePage from "../pages/WelcomePage";
import DetailPage from "../pages/DetailPage";

const InitNavigator = createStackNavigator({
  WelcomePage: {
    screen: WelcomePage,
    navigationOptions: {
      header: null, // 隐藏头部
    }
  }
});

const MainNavigator = createStackNavigator({
  HomePage: {
    screen: HomePage,
    navigationOptions: {
      header: null
    }
  },
  DetailPage: {
    screen: DetailPage
  }
});

export default createAppContainer(createSwitchNavigator({
  Init: InitNavigator,
  Main: MainNavigator
}, {
  navigationOptions: {
    header: null
  }
}));
