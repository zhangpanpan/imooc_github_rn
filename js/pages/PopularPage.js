import React, { Component } from "react";
import {View, Text, Button, StyleSheet} from 'react-native';
import {createMaterialTopTabNavigator} from 'react-navigation-tabs';
import {createAppContainer} from 'react-navigation';
import NavigationUtil from "../navigator/NavigationUtil";

class PopularPage extends Component {
  constructor(props) {
    super(props);
    this.tabNames = [
      'Java',
      'Android',
      'iOS',
      'React',
      'React Native',
      'PHP'
    ];
  }

  _genTabs() {
    const tabs = {};
    this.tabNames.forEach((item, index) => {
      tabs[`tab${index}`] = {
        screen: props => <PopularTab
          {...this.props}
          tabLabel={item}
        />,
        navigationOptions: {
          title: item,
        }
      };
    });
    return tabs;
  }

  render() {
    const TabNavigator = createAppContainer(createMaterialTopTabNavigator(
      this._genTabs(),
      {
        tabBarOptions: {
          tabStyle: styles.tabStyle,
          upperCaseLabel: false,
          scrollEnabled: true,
          style: {
            backgroundColor: '#a67'
          },
          indicatorStyle: styles.indicatorStyle,
          labelStyle: styles.labelStyle
        }
      }
    ));

    return (
      <View style={styles.container}>
        <TabNavigator />
        <Text style={styles.text}>Popular Page</Text>
      </View>
    );
  }
}

class PopularTab extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
        <View style={styles.container}>
          <Text>Popular Tab</Text>
          <Text onPress={() => {
            NavigationUtil.goPage({}, 'DetailPage');
          }}>
            跳转到详情页
          </Text>
        </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 44,
    backgroundColor: '#ffffff'
  },
  text: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10
  },
  tabStyle: {
    minWidth: 50
  },
  indicatorStyle: {
    height: 2,
    backgroundColor: '#ffffff'
  },
  labelStyle: {
    fontSize: 13,
    marginTop: 6,
    marginBottom: 6
  }
});

export default PopularPage;
