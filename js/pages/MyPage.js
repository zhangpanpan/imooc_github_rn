import React, { Component } from "react";
import {View, Text, Button, StyleSheet} from 'react-native';

class MyPage extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.text}>My Page</Text>
        <Button title={'修改主体'} onPress={() => {
          this.props.navigation.setParams({
            theme: {
              tintColor: 'red',
              updateTime: new Date().getTime()
            }
          });
        }}/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF'
  },
  text: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10
  }
});

export default MyPage;
