import React, {Component} from 'react';
import {View, Text, Button, SafeAreaView, StyleSheet} from 'react-native';
import NavigationUtil from "../navigator/NavigationUtil";

class WelcomePage extends Component {
  constructor(props) {
    super(props);
    this.timer = null;
  }

  componentDidMount() {
    this.timer = setTimeout(() => {
      // 跳转到首页
      NavigationUtil.resetToHomePage(this.props);
    }, 200);
  }

  componentWillUnmount() {
    this.timer = null;
  }

  render() {
    return (
      <View style={styles.container}>
        <Text>Welcome Page</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default WelcomePage;
